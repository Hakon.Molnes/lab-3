package no.uib.inf101.terminal;

import java.util.StringJoiner;
import java.util.List;

public class CmdEcho implements Command {

    @Override
    public String run(String[] args) {
        StringJoiner s = new StringJoiner(" ");
        for (String i : args){
            s.add(i);
        }
        String sp = s.toString();

        if (sp.isEmpty()){
            System.out.println("The list is empty.");
            return "";

        }
        else {
            String Fin = s + " ";
            return Fin;
        }
        
        

        /*Alternative
        for (String i : args){
            String joined = words.stream().collect(Collectors.joining(" "));
        }*/

    }

    @Override
    public String getName() {
        return "echo";
    }
    
}
